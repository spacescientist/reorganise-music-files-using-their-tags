# Objective
Read tags to reorganise music files in any folders, following the convention
```
    artist/album/track - title.extension
```

# Requirements
If mutagen (for handling music tags) is not installed yet, in your virtual environment, you can do:
```
    pip install mutagen
```
more info: https://mutagen.readthedocs.io
