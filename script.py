# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Read tags to reorganise music files in any folders, following
#     artist/album/track - title.extension

# +
# If not installed yet, in your virtual environment, you can do:
#    pip install mutagen

# +
output_folder = "./Music/" # set to w/e you want.



from mutagen.easyid3 import EasyID3
print(EasyID3.valid_keys.keys()) # FYI, lists all easy-to-use tag keys

import os
import glob

# for foldername in sorted(["./F00/"]):
# for foldername in sorted(glob.glob("./F*/")):
for foldername in sorted(glob.glob(f"{output_folder}/*/*/")):
        print("Input folder:", foldername)
        print()
        
        for filename in sorted(os.listdir(foldername)):
            print(filename)

            file_extension = os.path.splitext(filename)[1]
            print(file_extension)

            # It seems mutagen cannot deal with .m4a (can't blame it), so we'll just skip those.
            crappy_file_formats = [".m4a",] # don't forget the leading dot if you add extensions, btw

            if(file_extension not in crappy_file_formats):

                # Opening the file with the library:
                audiofile = EasyID3(f"{foldername}{filename}")

                # NB: we don't edit the tags in any way here; we only retrieve the information.
                
                # artist
                try:
                    artist = audiofile.get('artist')[0].strip() # strip is to remove potential trailing spaces
                except:
                    artist = audiofile.get('albumartist')[0].strip()

                # title
                title = audiofile.get('title')[0].strip()
                title = title.replace('/', '-')
                title = title.replace('[', '(')
                title = title.replace(']', ')')

                # album
                album = audiofile.get('album')[0].strip()
                album = album.replace('/', '-')
                album = album.replace('[', '(')
                album = album.replace(']', ')')

                # track number
                try:
                    tracknumber = str(audiofile.get('tracknumber')[0].strip()).split("/")[0] # we may have track 1/10 stored as tag...
                except:
                    tracknumber = ""



                # OK, let's create our folders based on those ^:
                print(f"{artist}/{album}/")

                try:
                    os.mkdir(f"{output_folder}/{artist}")
                except:
                    pass
                try:
                    os.mkdir(f"{output_folder}/{artist}/{album}")
                except:
                    pass

                # OK, time to move our files according to the tags:
                print(f"{tracknumber} - {title}{file_extension}")

                os.rename(f"{foldername}{filename}", f"./{output_folder}/{artist}/{album}/{tracknumber.rjust(2, '0')} - {title}{file_extension}")

                print()
                print()
# -


